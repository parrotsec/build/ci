#!/bin/bash
for file in $@; do
	export amd64=0
	export i386=0
	export arm64=0
	export armhf=0
	architectures=$(grep '^Architecture' $file | sed -e 's/Architecture: //')
	for arch in $architectures; do
		case $arch in
			all)
				export amd64=1
			;;
			any|linux-any)
				export amd64=1
				export i386=1
				export arm64=1
				export armhf=1
			;;
			amd64|any-amd64)
				export amd64=1
			;;
			i386|any-i386)
				export i386=1
			;;
			arm64)
				export arm64=1
			;;
			armhf)
				export armhf=1
			;;
		esac
	done
	echo amd64 $amd64
	echo i386 $i386
	echo arm64 $arm64
	echo armhf $armhf

	if [ $amd64 -eq 1 ]; then
		sbuild --arch=amd64 --chroot=lory-amd64 --no-run-lintian --no-run-autopkgtest --arch-all --arch-any --no-source $file
	fi
	if [ $arm64 -eq 1 ]; then
                sbuild --arch=arm64 --chroot=lory-arm64 --no-arch-all --no-run-autopkgtest --arch-any --no-run-lintian --no-source $file
        fi
	if [ $i386 -eq 1 ]; then
                sbuild --arch=i386 --chroot=lory-i386 --no-arch-all --no-run-autopkgtest --arch-any --no-run-lintian --no-source $file
        fi
	if [ $armhf -eq 1 ]; then
                sbuild --arch=armhf --chroot=lory-armhf --no-arch-all --no-run-autopkgtest --arch-any --no-run-lintian --no-source $file
        fi
done